import React, { Component } from 'react';
import{
  BrowserRouter as Router,
  Route,
  Link
}from 'react-router-dom';
import Header from './components/headerComponent/header';
import Homepage from './components/pages/homePage';
import Contact from './components/pages/contact';
import './Assets/css/default.min.css';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <Header></Header>
          <Route exact path='/' component={Homepage}/>
          <Route exact path='/Contact' component={Contact} />
      </div>
      </Router>
    );
  }
}

export default App;
