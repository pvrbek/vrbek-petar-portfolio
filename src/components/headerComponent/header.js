import React, { Component } from 'react';
import logo from '../../Images/logo.jpg';
import {
    Link
} from 'react-router-dom';
class Header extends Component {
    render() {
        return (
            <header>
                <div className="logo">
                    <img src={logo} alt="logoImage"/>
                </div>
                <nav>
                    <ul>
                        <li className="first"> 
                            <Link to="/">Petar Vrbek Portfolio</Link>
                        </li>
                        <li className="last">
                            <Link to="/Contact">Contact</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;
