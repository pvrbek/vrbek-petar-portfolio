import React, { Component } from 'react';

class Homepage extends Component {
    render() {
        return (
            <div className="container-fluid">
                <h1>WORKING EXPERIENCE</h1>
                <h2>Trikoder</h2>
                <nav>
                    <ul>
                        <li>Student traineeship</li>
                        <li>2016 (1 month)</li>
                        <li>Front-End Development</li>
                    </ul>
                </nav>
                <h2>Croatia Airlines</h2>
                <nav>
                    <ul>
                        <li>2016-2017</li>
                        <li>System Analyst</li>
                    </ul>
                </nav>
                <h2>Croatia Airlines</h2>
                <nav>
                    <ul>
                        <li>2017-present</li>
                        <li>System Specialist</li>
                    </ul>
                </nav>
                <h1>EDUCATION</h1>
                <h2>Technical school Ruđer Bošković</h2>
                <nav>
                    <ul>
                        <li>2007-2011</li>
                        <li>Computer technician</li>
                    </ul>
                </nav>
                <h2>Faculty of Organization and Informatics, Uneversity of Zagreb</h2>
                <nav>
                    <ul>
                        <li>2011-2014</li>
                        <li>Bachelor's degree, Information Technology</li>
                    </ul>
                </nav>
                <h2>Faculty of Organization and Informatics, Uneversity of Zagreb</h2>
                <nav>
                    <ul>
                        <li>2014-2016</li>
                        <li>Master's degree, Software Engineering</li>
                    </ul>
                </nav>
                <h1>TECHNICAL SKILLS</h1>
                <nav>
                    <ul>
                        <li>HTML</li>
                        <li>CSS</li>
                        <li>SCSS</li>
                        <li>JavaScript</li>
                        <li>jQuery</li>
                        <li>Java</li>
                        <li>Git</li>
                        <li>MVC Pattern</li>
                        <li>PostgreSQL</li>
                        <li>IBM Netezza SQL</li>
                        <li>IBM Cognos BI Development (Report Studio)</li>
                        <li>IBM Cognos BI User Administration </li>
                        <li>SAP User Administration</li>
                    </ul>
                </nav>
                <h1>Professional Interests</h1>
                <nav>
                    <ul>
                        <li>Web Development</li>
                        <li>Mobile Development</li>
                        <li>Business Intelligence Development</li>
                        <li>Big Data</li>
                        <li>IoT</li>
                        <li>Artificial Intelligence</li>
                        <li>Blockchain Technology</li>
                        <li>Project Management</li>
                    </ul>
                </nav>
                <h1>Private Interests</h1>
                <nav>
                    <ul>
                        <li>Dogs, cats and all other animals</li>
                        <li>Beer</li>
                        <li>Pizza and burgers</li>
                        <li>Extreme sports</li>
                        <li>Board games with friends</li>
                        <li>Traveling</li>
                    </ul>
                </nav>
            </div>
        );
    }
}

export default Homepage;